const knex = require('knex')({
  client: 'mysql',
  debug: false,
  connection: {
    host: process.env.HOST_DB,
    port: process.env.PORT_DB,
    user: process.env.USER_DB,
    password: process.env.PASSWORD_DB,
    database: process.env.NAME_DB,
  },
});

module.exports = knex;
