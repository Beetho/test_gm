/* eslint-disable class-methods-use-this */
const knex = require('../database/db');

class CitiesDAO {
  async searchCitiesByName(cityName) {
    return knex('cities').whereLike('asciiname', `%${cityName}%`);
  }
}

module.exports = CitiesDAO;
