const express = require('express');

const router = express.Router();
const { getCitiesByName } = require('./controllers/cities_controller');

router.get('/search', getCitiesByName);

module.exports = router;
