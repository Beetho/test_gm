const express = require('express');

const app = express();
require('dotenv').config();

app.use('/', require('./router'));

app.listen(process.env.PORT_LISTEN, () => {
  console.log('Server running');
});
