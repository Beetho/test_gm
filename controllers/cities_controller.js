const CitiesDAO = require('../daos/cities_DAO');
const { calculatorDistance, getScoreResult } = require('./utils/calculator_distance');

function isNum(val) {
  return !Number.isNaN(val);
}

exports.getCitiesByName = async (req, resp) => {
  const {
    q: citySearch,
    latitude: latitudeInput,
    longitude: longitudeInput,
  } = req.query;
  try {
    if (!citySearch) {
      return resp.status(400).json({ message: "Missing 'q' query parameter" });
    }

    if (latitudeInput) {
      if (!isNum(latitudeInput)) {
        return resp.status(400).json({ message: "Query parameter 'latitude' is not type numeric" });
      }
    }
    if (longitudeInput) {
      if (!isNum(longitudeInput)) {
        return resp.status(400).json({ message: "Query parameter 'longitude' is not type numeric" });
      }
    }

    const citiesDAO = new CitiesDAO();
    const citiesResult = await citiesDAO.searchCitiesByName(citySearch);
    if (citiesResult.length === 0) {
      return resp.status(200).json({ search: [] });
    }
    const citiesMap = citiesResult.map((city) => {
      // distance in KM
      const distance = calculatorDistance(
        latitudeInput,
        longitudeInput,
        city.latitude,
        city.longitude,
      );
      const cityJson = {
        name: `${city.asciiname}, ${city.country_code}`,
        latitude: city.latitude,
        longitude: city.longitude,
        score: getScoreResult(distance, citySearch, city.asciiname),
      };
      return cityJson;
    });
    citiesMap.sort((a, b) => b.score - a.score);
    const cities = {
      search: citiesMap,
    };
    return resp.status(200).json(cities);
  } catch (parseErr) {
    console.log('parseErr:', parseErr);
    return resp.status(500).json(parseErr);
  }
};
