// Converts numeric degrees to radians
function toRad(Value) {
  return (Value * Math.PI) / 180;
}

/*
  This function takes in latitude and longitude of two
  location and returns the distance between them as the crow flies (in km)
*/
function calculatorDistance(latInput, lonInput, latDB, lonDB) {
  const R = 6371; // km
  const dLat = toRad(latDB - latInput);
  const dLon = toRad(lonDB - lonInput);
  const lat1 = toRad(latInput);
  const lat2 = toRad(latDB);

  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
    + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c;
  return d;
}

function getScoreResult(distance, cityNameInput, citiNameResult) {
  const percentScoreDistance = 0.7; // 70%
  const percentNameCoincidence = 0.3; // 30%
  let scoreValue = 0;
  switch (true) {
    case (distance <= 100): scoreValue = 1; break;
    case (distance > 100 && distance <= 200): scoreValue = 0.9; break;
    case (distance > 200 && distance <= 300): scoreValue = 0.8; break;
    case (distance > 300 && distance <= 400): scoreValue = 0.7; break;
    case (distance > 400 && distance <= 500): scoreValue = 0.6; break;
    case (distance > 500 && distance <= 600): scoreValue = 0.5; break;
    case (distance > 600 && distance <= 700): scoreValue = 0.4; break;
    case (distance > 700 && distance <= 800): scoreValue = 0.3; break;
    case (distance > 800 && distance <= 900): scoreValue = 0.2; break;
    case (distance > 900 && distance <= 1000): scoreValue = 0.1; break;
    default: scoreValue = 0; break;
  }
  // formula
  const score = parseFloat(
    (
      percentScoreDistance * scoreValue
      + (cityNameInput === citiNameResult ? percentNameCoincidence : 0)
    ).toFixed(1),
  );
  return score;
}
module.exports = { calculatorDistance, getScoreResult };
