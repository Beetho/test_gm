# test-gm

_Prueba: creacion y consumo de endpoint de un buscador de ciudades de US y Canada_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Antes de comenzar, se debe tomar en cuenta tener instalado lo siguiente_

* [Node js](https://nodejs.org/es/) 
* [Mysql](https://dev.mysql.com/downloads/installer/) 


### Instalación de dependencias 🔧

_Para el uso correcto, se debe instalar las siguientes dependencias_

```
npm install
```

## Creación y carga de la base de datos

_Dentro de la carpeta database (**database/scripts**) se encuentra los siguientes archivos_
_Ejecutar en tu administrador de base de datos (MySQL) en el orden indicado_
```
1. test_db.sql
2. cities_ca.sql
3. cities_us.sql
```

## Variables de entorno ⚙️

_En la raíz del proyecto, se encuentra el archivo .env con los datos de configuración para ejecutarlo localmente_

```
HOST_DB="127.0.0.1"
PORT_DB=3306
USER_DB="root"
PASSWORD_DB=""
NAME_DB="test_db"

PORT_LISTEN: 3000
```
## Ejecutando en local ⌨️

_Para la ejecución localmente sigue los siguiengtes pasos_

* Abrir la consola
* Ubicarte en la raiz de tu proyecto
```
Por ejemplo: C:\Projects\test-gm
```
* Ejecutar la siguiente linea
```
npm start 
Ejemplo: C:\Projects\test-gm> npm start
```
* Con ello podremos usar http://localhost:3000

### Consumo de endpoint

_Para el uso y consumo del endpoint de busqueda de ciudades seguir las siguientes instrucciones_


* Endpoint de busqueda /search será de tipo GET
```
http://localhost:3000/search
```

* Como dato de entrada requerido tenemos el query parameter "q" que es usada para la busqueda de las Ciudades, este dato es obligatorio

| Datos de entrada | Tipo:Requerido | Descripcion
| ------------- | ------------- | ------------- |
| q  | String:true  | se coloca el nombre de la ciudad a buscar |
| latitude  | Numeric:false  | es la latitude de la ciudad a buscar |
| longitude  | Numeric:false  | es la longitude de la ciudad a buscar |

* Ejemplo de peticion solo con parametro "q"
```
http://localhost:3000/search?q=Montreal
```

* Ejemplo de Salida
```
{
	"search": [
		{
			"name": "Montreal, CA",
			"latitude": 45.50008,
			"longitude": -73.68248,
			"score": 0.3
		}
	]
}
```

* También puedes agregar a la busqueda latitude y longitude
```
http://localhost:3000/search?q=Montreal&latitude=45.50008&longitude=-70.68248
```


* Ejemplo de Salida
```
{
	"search": [
		{
			"name": "Montreal, CA",
			"latitude": 45.50008,
			"longitude": -73.68248,
			"score": 0.9
		}
	]
}
```
* Peticion ejemplo de una ciudad no existente
```
http://localhost:3000/search?q=SomeRandomCityInTheMiddleOfNowhere
```

_Si la ciudad no fue encontrada, responderá con lo siguiente_

```
{
	"search": []
}
```

* Peticion ejemplo cuando el parametro "q" no es enviada
```
http://localhost:3000/search
```

_Respuesta de error con status 400_

```
{
	"message": "Missing 'q' query parameter"
}
```


## Pruebas unitarias 🛠️

_*Por el momento las pruebas unitarias no estan disponibles en esta versión _


---
⌨️ created by [Alberto Garcia - Beetho](https://gitlab.com/Beetho)
